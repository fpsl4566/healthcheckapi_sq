package com.sq.healthcheckapi.controller;

import com.sq.healthcheckapi.model.HealthItem;
import com.sq.healthcheckapi.model.HealthRequest;
import com.sq.healthcheckapi.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths() {
        return healthService.getHealths();
    }
}
