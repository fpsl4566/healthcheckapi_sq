package com.sq.healthcheckapi.repository;

import com.sq.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health, Long> {
}
